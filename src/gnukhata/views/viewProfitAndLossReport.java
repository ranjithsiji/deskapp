package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.reportController;
import gnukhata.controllers.reportmodels.ProfitAndLossReport;
import gnukhata.controllers.reportmodels.netTrialBalance;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

public class viewProfitAndLossReport extends Composite {
	
	int counter = 0;
	static Display display;
	TableViewer tblProfitAndLoss;
	

	Button btnViewplForAccount;
	Button btnPrint;
	NumberFormat nf;
	ODPackage  sheetStream;
	ODPackage  iesheetStream;
	int shellwidth = 0;
	Vector<Object> printPl = new Vector<Object>();
	//ArrayList<Button> accounts = new ArrayList<Button>();
	String toDateParam = ""; 
	public viewProfitAndLossReport(Composite parent, int style, String toDate,ArrayList<gnukhata.controllers.reportmodels.ProfitAndLossReport >  pandlData)
	{
		super(parent,style);
		//endDateParam = endDate;
		FormLayout formlayout = new FormLayout();
		FormData layout=new FormData();
		this.setLayout(formlayout);
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(87);
		layout.bottom = new FormAttachment(9);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		lblLogo.setLocation(getClientArea().width, getClientArea().height);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		/*Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);*/
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment( lblLogo , 2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
		
		

		
		Label lblheadline=new Label(this, SWT.NONE);
		String strdate;
		strdate=toDate.substring(8)+"-"+toDate.substring(5,7)+"-"+toDate.substring(0, 4);
		toDateParam = strdate;
		if(globals.session[4].equals("profit making"))
		{
			lblheadline.setText("Profit and Loss Account For The Period "+"From "+globals.session[2]+" To "+strdate);
		}
		if(globals.session[4].equals("ngo"))
		{
			lblheadline.setText("Income and Expenditure Account For The Period "+"From "+globals.session[2]+" To "+strdate);
		}
		
		lblheadline.setFont(new Font(display, "Times New Roman", 12, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,2);
		layout.left = new FormAttachment(22);
		lblheadline.setLayoutData(layout);
		
		Label exp=new Label(this,SWT.NONE);
		exp.setFont(new Font(display, "Times New Roman", 12, SWT.ITALIC| SWT.BOLD));
		exp.setText("EXPENDITURE");
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,2);
		layout.left = new FormAttachment(20);
		exp.setLayoutData(layout);
		
		Label inc=new Label(this,SWT.NONE);
		inc.setFont(new Font(display, "Times New Roman", 12, SWT.ITALIC| SWT.BOLD));
		inc.setText("INCOME");
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,2);
		layout.left = new FormAttachment(65);
		inc.setLayoutData(layout);
		

			

		//table1
		tblProfitAndLoss = new TableViewer(this, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION|SWT.LINE_SOLID);
		tblProfitAndLoss.getTable().setFont(new Font(display,"UBUNTU",10,SWT.BOLD));
		tblProfitAndLoss.getTable().setLinesVisible (true);
		tblProfitAndLoss.getTable().setHeaderVisible (true);
		layout = new FormData();
		layout.top = new FormAttachment(inc,10);
		layout.left = new FormAttachment(15);
		layout.right = new FormAttachment(85);
		layout.bottom = new FormAttachment(92);
		tblProfitAndLoss.getTable().setLayoutData(layout);
		
		    btnViewplForAccount =new Button(this,SWT.PUSH);
		   if(globals.session[4].equals("profit making"))
			{
				btnViewplForAccount.setText("&Back to Profit and Loss");
			}
			if(globals.session[4].equals("ngo"))
			{
				btnViewplForAccount.setText("&Back to Income and Expenditure ");
			}
			 btnViewplForAccount.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
			layout = new FormData();
			layout.top=new FormAttachment(tblProfitAndLoss.getTable(),15);
			layout.left=new FormAttachment(35);
			btnViewplForAccount.setLayoutData(layout);

		
		   btnPrint =new Button(this,SWT.PUSH);
			btnPrint.setText(" &Print ");
			btnPrint.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
			layout = new FormData();
			layout.top=new FormAttachment(tblProfitAndLoss.getTable(),15);
			layout.left=new FormAttachment(60);
			btnPrint.setLayoutData(layout);

		
			this.getAccessible();
			//this.setEvents();
			//this.pack();
			this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
			shellwidth = this.getClientArea().width;
			
			
			this.setReport(pandlData);
			setEvents(pandlData);
			
	}
	

	
	private void setReport(ArrayList<gnukhata.controllers.reportmodels.ProfitAndLossReport > pandlData )
	{

			
		final TableViewerColumn colto= new TableViewerColumn(tblProfitAndLoss, SWT.None);
		colto.getColumn().setText(" ");
		colto.getColumn().setAlignment(SWT.LEFT);
		colto.getColumn().setWidth(2* shellwidth /100);
		

		colto.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				
				gnukhata.controllers.reportmodels.ProfitAndLossReport pl = (gnukhata.controllers.reportmodels.ProfitAndLossReport) element;

				return pl.getto();
				//return super.getText(element);
			}
		}
		);
		
		
		final TableViewerColumn colaccname= new TableViewerColumn(tblProfitAndLoss, SWT.None);
		colaccname.getColumn().setText("                          Account Name ");
		colaccname.getColumn().setAlignment(SWT.LEFT);
		colaccname.getColumn().setWidth(21* shellwidth /100);
		
		colaccname.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.ProfitAndLossReport pl = (gnukhata.controllers.reportmodels.ProfitAndLossReport) element;
				return pl.getaccountname();
				//return super.getText(element);
			}
		}
		);
		
		final TableViewerColumn colamount= new TableViewerColumn(tblProfitAndLoss, SWT.None);
		colamount.getColumn().setText("Amount          ");
		colamount.getColumn().setAlignment(SWT.RIGHT);
		colamount.getColumn().setWidth(10* shellwidth /100);
		
		colamount.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.ProfitAndLossReport pl = (gnukhata.controllers.reportmodels.ProfitAndLossReport) element;
				try {
					Double amount = Double.parseDouble(pl.getamount());
					nf = NumberFormat.getInstance();
					nf.setGroupingUsed(false);
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					return nf.format(amount);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					return "";
					
				}
			}
		}
		);
		
		
		final TableViewerColumn colby = new TableViewerColumn(tblProfitAndLoss, SWT.None);
		colby.getColumn().setText(" ");
		colby.getColumn().setAlignment(SWT.LEFT);
		colby.getColumn().setWidth(2* shellwidth /100);
		
		colby.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.ProfitAndLossReport pl = (gnukhata.controllers.reportmodels.ProfitAndLossReport) element;
				return pl.getby();
				//return super.getText(element);
			}
		}
		);
		
		
		
		final TableViewerColumn colaccname1 = new TableViewerColumn(tblProfitAndLoss, SWT.None);
		colaccname1.getColumn().setText("                          Account Name ");
		colaccname1.getColumn().setAlignment(SWT.LEFT);
		colaccname1.getColumn().setWidth(21* shellwidth /100);
		
		colaccname1.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.ProfitAndLossReport pl = (gnukhata.controllers.reportmodels.ProfitAndLossReport) element;
				return pl.getaccountname1();
				//return super.getText(element);
			}
		}
		);
		
			
		final TableViewerColumn colamount1 = new TableViewerColumn(tblProfitAndLoss, SWT.None);
		colamount1.getColumn().setText("Amount            ");
		colamount1.getColumn().setAlignment(SWT.RIGHT);
		colamount1.getColumn().setWidth(10* shellwidth /100);
		
		colamount1.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.ProfitAndLossReport pl = (gnukhata.controllers.reportmodels.ProfitAndLossReport) element;
				try {
					Double amount1 = Double.parseDouble(pl.getamount1());
					nf = NumberFormat.getInstance();
					nf.setGroupingUsed(false);
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					return nf.format(amount1);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block

					e.printStackTrace();
					return "";
				}

			}
		}
		);
		
		
		
		
		 tblProfitAndLoss.setContentProvider(new ArrayContentProvider());
		 tblProfitAndLoss.setInput(pandlData);
		 
		
		tblProfitAndLoss.getTable().setFocus();		
	}

	
	private void setEvents(final ArrayList<gnukhata.controllers.reportmodels.ProfitAndLossReport > pandlData  )
	{
		
	
		btnViewplForAccount.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnPrint.setFocus();
				}
				
			}
		});
		
		
		btnPrint.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnViewplForAccount.setFocus();
				}
				
			}
		});
		
		btnViewplForAccount.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnViewplForAccount.getParent().getParent();
				btnViewplForAccount.getParent().dispose();
					
					viewProfitAndLoss vp=new viewProfitAndLoss(grandParent,SWT.NONE);
					vp.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
		
		});
				

		}


	
	
	
	public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}



	protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}
	/*public static void main(String[] args)
	{
		Display d = new Display();
		Shell s= new Shell(d);
		/*int vouchercode = 0;
		String voucherType = null;
		viewTrialBalReport vtbr=new viewTrialBalReport(s, SWT.NONE);
		vtbr.setSize(s.getClientArea().width, s.getClientArea().height );
		
		//s.setSize(400, 400);
		s.pack();
		s.open();
		while (!s.isDisposed() ) {
			if (!d.readAndDispatch())
			{
				 d.sleep();
				 if(! s.getMaximized())
				 {
					 s.setMaximized(true);
				 }
			}
		}
		
	}*/
}
