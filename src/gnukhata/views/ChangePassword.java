package gnukhata.views;

import java.util.HashMap;

import gnukhata.globals;
import gnukhata.controllers.StartupController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.sun.org.apache.xerces.internal.impl.dtd.models.CMUniOp;

public class ChangePassword extends Composite{
	static Display display;
	String strOrgName;
	String strFromYear;
	String strToYear;
	String strype;
	Label lblusername;
	Label lbloldpassword;
	Label lblnewpassword;
	Label lblconfirmpassword;
	Text txtusername;
	Text txtoldpassword;
	Text txtnewpassword;
	Text txtconfirmpassword;
	Button btnsave;
	Button btncancel;
	Button[] btnchangepassword;
	Combo cmbusers;
	
	String struser;
	
	HashMap<String, String> hashuser= new HashMap<>(); 
	
	public ChangePassword(Composite parent,int style) {
		super(parent, style);
		
		
		FormLayout formlayout =new FormLayout();
		this.setLayout(formlayout);
		FormData layout = new FormData();
		
		MainShell.lblLogo.setVisible(false);
		MainShell.lblLine.setVisible(false);
		MainShell.lblOrgDetails.setVisible(false);
		
		strToYear =  globals.session[3].toString();
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(87);
		layout.bottom = new FormAttachment(9);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		lblLogo.setLocation(getClientArea().width, getClientArea().height);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New romen", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		/*Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New romen", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);*/
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New romen",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment( lblLogo , 2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
		

		Label lblchangepass = new Label(this, SWT.NONE);
		lblchangepass.setText("Change Password");
		lblchangepass.setFont(new Font(display, "Times New romen", 12, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		layout.left = new FormAttachment(40);
		//layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(36);
		lblchangepass.setLayoutData(layout);
		
		
		Label lblloginuser = new Label(this, SWT.NONE);
		//lblloginuser.setText("You have logged in as " +globals.session[6]);
		lblloginuser.setFont(new Font(display, "Times New romen", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblchangepass,10);
		layout.left = new FormAttachment(38);
		//layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(36);
		lblloginuser.setLayoutData(layout);
		
		if(globals.session[7].equals(-1))
		{
			lblloginuser.setText("You have logged in as  Admin");
		}
		if(globals.session[7].equals(0))
		{
			lblloginuser.setText("You have logged in as  Manager");
		}
		if(globals.session[7].equals(1))
		{
			lblloginuser.setText("You have logged in as  Operator");
		}
		
		
		btnchangepassword = new Button[2];
		btnchangepassword[0] = new Button(this, SWT.RADIO);
		btnchangepassword[0].setSelection(false);
		btnchangepassword[0].setText("O&wn");
		btnchangepassword[0].setFont(new Font(display, "Times New romen", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblloginuser,12);
		layout.left = new FormAttachment(40);
		btnchangepassword[0].setLayoutData(layout);
		//btnchangepassword[0].setVisible(false);
		btnchangepassword[0].setFocus();
			
		btnchangepassword[1] = new Button(this, SWT.RADIO);
		btnchangepassword[1].setSelection(false);
		btnchangepassword[1].setText("&Others");
		btnchangepassword[1].setFont(new Font(display, "Times New romen", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblloginuser,12);
		layout.left = new FormAttachment(btnchangepassword[0],7);
		//layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(36);
		btnchangepassword[1].setLayoutData(layout);
		//btnchangepassword[1].setVisible(false);
		
			
		lblusername = new Label(this, SWT.NONE);
		lblusername.setText("&Username:");
		lblusername.setFont(new Font(display, "Times New romen", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(btnchangepassword[0],20);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(36);
		lblusername.setLayoutData(layout);
		lblusername.setVisible(false);
		
		txtusername=new Text(this, SWT.BORDER);
		txtusername.setFont(new Font(display, "Times New romen", 10, SWT.READ_ONLY));
		layout = new FormData();
		layout.top = new FormAttachment(btnchangepassword[0],20);
		layout.left = new FormAttachment(51);
		layout.right = new FormAttachment(62);
		//layout.bottom = new FormAttachment(48);
		txtusername.setEditable(false);
		txtusername.setLayoutData(layout);
		txtusername.setVisible(false);
		
		cmbusers = new Combo(this, SWT.DROP_DOWN | SWT.READ_ONLY);
		cmbusers.setFont(new Font(display,"Times New Roman",10,SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(btnchangepassword[0],20);
		layout.left = new FormAttachment(51);
		layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(14);
		cmbusers.setLayoutData(layout);
		
		cmbusers.add("--Please select--");
		cmbusers.select(0);
		cmbusers.setVisible(false);
		
		
		lbloldpassword = new Label(this, SWT.NONE);
		lbloldpassword.setText("&Old Password :");
		lbloldpassword.setFont(new Font(display, "Times New Romn", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblusername,10);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(53);
		lbloldpassword.setLayoutData(layout);
		lbloldpassword.setVisible(false);
		
		txtoldpassword = new Text(this, SWT.BORDER);
		txtoldpassword.setFont(new Font(display, "Times New romen", 10, SWT.NONE));
		txtoldpassword.setEchoChar('*');
		layout = new FormData();
		layout.top = new FormAttachment(lblusername,10);
		layout.left = new FormAttachment(51);
		layout.right = new FormAttachment(62);
		//layout.bottom = new FormAttachment(53);
		//txtoldpassword.setEditable(false);
		txtoldpassword.setLayoutData(layout);
		txtoldpassword.setVisible(false);
		
		lblnewpassword = new Label(this, SWT.NONE);
		lblnewpassword.setText("&New Password :");
		lblnewpassword.setFont(new Font(display, "Times New romen", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lbloldpassword,10);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(58);
		lblnewpassword.setLayoutData(layout);
		lblnewpassword.setVisible(false);
		
		txtnewpassword = new Text(this, SWT.BORDER);
		txtnewpassword.setFont(new Font(display, "Times New romen", 10, SWT.NONE));
		txtnewpassword.setEchoChar('*');
		layout = new FormData();
		layout.top = new FormAttachment(lbloldpassword,10);
		layout.left = new FormAttachment(51);
		layout.right = new FormAttachment(62);
		//layout.bottom = new FormAttachment(58);
		txtnewpassword.setLayoutData(layout);	
		txtnewpassword.setVisible(false);
		
		lblconfirmpassword = new Label(this, SWT.NONE);
		lblconfirmpassword.setText("Confirm &Password:");
		lblconfirmpassword.setFont(new Font(display, "Times New romen", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblnewpassword,10);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(63);
		lblconfirmpassword.setLayoutData(layout);
		lblconfirmpassword.setVisible(false);
		
		txtconfirmpassword = new Text(this, SWT.BORDER);
		txtconfirmpassword.setFont(new Font(display, "Times New romen", 10, SWT.NONE));
		txtconfirmpassword.setEchoChar('*');
		layout = new FormData();
		layout.top = new FormAttachment(lblnewpassword,10);
		layout.left = new FormAttachment(51);
		layout.right = new FormAttachment(62);
		//layout.bottom = new FormAttachment(63);
		txtconfirmpassword.setLayoutData(layout);
		txtconfirmpassword.setVisible(false);
		
		btnsave = new Button(this,SWT.PUSH);
		btnsave.setText("&Save");
		btnsave.setFont(new Font(display, "Times New romen", 12, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblconfirmpassword,20);
		layout.left = new FormAttachment(40);
	//	layout.right = new FormAttachment(50);
	//	layout.bottom = new FormAttachment(70);
		btnsave.setLayoutData(layout);
		btnsave.setVisible(false);
		
		btncancel = new Button(this,SWT.PUSH);
		btncancel.setText("&Cancel");
		btncancel.setFont(new Font(display, "Times New romen", 12, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblconfirmpassword,20);
		layout.left = new FormAttachment(53);
	//	layout.right = new FormAttachment(65);
	//	layout.bottom = new FormAttachment(70);
		btncancel.setLayoutData(layout);
	
	
		this.pack();
		this.getAccessible();
		this.setEvents();
			
	}
	private void setEvents()
	{
		
		if(globals.session[7].equals(1))
		{
			
			btnchangepassword[0].setVisible(false);
			btnchangepassword[1].setVisible(false);
			lblusername.setVisible(true);
			txtusername.setVisible(true);
			txtusername.setText(globals.session[6].toString());
			lbloldpassword.setVisible(true);
			txtoldpassword.setVisible(true);
			lblnewpassword.setVisible(true);
			txtnewpassword.setVisible(true);
			lblconfirmpassword.setVisible(true);
			txtconfirmpassword.setVisible(true);
			btnsave.setVisible(true);
			txtoldpassword.setFocus();
		}
	//	txtusername.setFocus();
		txtusername.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
						if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.KEYPAD_CR)
						{
							
							if(txtoldpassword.getVisible()==true)
							{
								txtoldpassword.setFocus();
							}
							else
							{
								txtnewpassword.setFocus();
							}
							
						}
						

			}
				
			
		});
		
		txtusername.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyReleased(arg0);
				if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.KEYPAD_CR)
				{
					if(txtoldpassword.getVisible()==true)
					{
						txtoldpassword.setFocus();
					}
					else
					{
						txtnewpassword.setFocus();
					}
					
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					
						btnchangepassword[0].setFocus();
					
				}
			}
		});
		
		btnchangepassword[0].addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyReleased(arg0);
				if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.KEYPAD_CR)
				{
					
						txtoldpassword.setFocus();
					
				}
			}
		});
		
		btnchangepassword[1].addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyReleased(arg0);
				if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.KEYPAD_CR)
				{
					
						cmbusers.setFocus();
					
				}
			}
		});
		

		txtoldpassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.KEYPAD_CR)
				{
					
						txtnewpassword.setFocus();
					
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					btnchangepassword[0].setFocus();
				}

				
					}
				
			
		});
		
		
		cmbusers.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.KEYPAD_CR)
				{
					if(cmbusers.getSelectionIndex()==0)
					{
						MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
						alert.setText("Error!");
						alert.setMessage("Please select a User");
						alert.open();
						cmbusers.setFocus();
						return;
					}
					else
					{
						txtnewpassword.setFocus();
					}
				}
				if(arg0.keyCode==SWT.ARROW_UP && cmbusers.getSelectionIndex() ==0)
				{
						btnchangepassword[0].setFocus();
					
				}

				
			}
		});
	
		txtnewpassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.KEYPAD_CR)
				{
					
						txtconfirmpassword.setFocus();
					
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					if(cmbusers.getVisible()==true)
					{
						cmbusers.setFocus();
					}
					else
					{
						txtoldpassword.setFocus();
					}
					
					
				}

				
			}
		});
		txtconfirmpassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.KEYPAD_CR)
				{
					
						btnsave.setFocus();
					
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtnewpassword.setFocus();
				}
			}
		});
		btnsave.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btncancel.setFocus();
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtconfirmpassword.setFocus();
				}
			}
		});
		btncancel.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnsave.setFocus();
				}
			}
		});
		
		
			btnchangepassword[0].addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					if(btnchangepassword[0].getSelection()==true)
					{
						lblusername.setVisible(true);
						txtusername.setVisible(true);
						txtusername.setText(globals.session[6].toString());
						lbloldpassword.setVisible(true);
						txtoldpassword.setVisible(true);
						lblnewpassword.setVisible(true);
						txtnewpassword.setVisible(true);
						lblconfirmpassword.setVisible(true);
						txtconfirmpassword.setVisible(true);
						btnsave.setVisible(true);
					}
					else
					{
						lblusername.setVisible(false);
						txtusername.setVisible(false);
						lbloldpassword.setVisible(false);
						txtoldpassword.setVisible(false);
						lblnewpassword.setVisible(false);
						txtnewpassword.setVisible(false);
						lblconfirmpassword.setVisible(false);
						txtconfirmpassword.setVisible(false);
						btnsave.setVisible(false);
					}
				}
			});
			
			btnchangepassword[1].addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					if(btnchangepassword[1].getSelection()==true)
					{
						lblusername.setVisible(true);
						cmbusers.setVisible(true);
						String username = globals.session[6].toString();
						String[] allusers=gnukhata.controllers.StartupController.getAllusers();
						for (int i = 0; i < allusers.length; i++) 
						{
							struser= allusers[i];
							String struserrole=gnukhata.controllers.StartupController.getuserrole(struser);
							cmbusers.add(struser +"-"+ struserrole);
						}
						lblnewpassword.setVisible(true);
						txtnewpassword.setVisible(true);
						lblconfirmpassword.setVisible(true);
						txtconfirmpassword.setVisible(true);
						btnsave.setVisible(true);
					}
					else
					{
						lblusername.setVisible(false);
						cmbusers.setVisible(false);
						lblnewpassword.setVisible(false);
						txtnewpassword.setVisible(false);
						lblconfirmpassword.setVisible(false);
						txtconfirmpassword.setVisible(false);
						btnsave.setVisible(false);
					}
				}
			});
	
	

		
		btnsave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				if(cmbusers.isVisible()==true)
				{
					if (cmbusers.getSelectionIndex()==0)
					{
						MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
						alert.setText("Error!");
						alert.setMessage("Please select a User");
						alert.open();
						cmbusers.setFocus();
						return;
					}
				}
					
					if(txtoldpassword.getVisible()==true)
					{
						if (txtoldpassword.getText().equals(""))
						{
							MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
							alert.setText("Error!");
							alert.setMessage("Please enter Old Password");
							alert.open();
							txtoldpassword.setFocus();
							return;
						}
					}
						
						if (txtnewpassword.getText().equals(""))
						{
							MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
							alert.setText("Error!");
							alert.setMessage("Please enter New Password");
							alert.open();
							txtnewpassword.setFocus();
							return;
						}
						if (txtconfirmpassword.getText().equals(""))
						{
							MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
							alert.setText("Error!");
							alert.setMessage("Please Confirm your New Password");
							alert.open();
							txtconfirmpassword.setFocus();
							return;
						}
						if(! txtnewpassword.getText().equals(txtconfirmpassword.getText()))
						{
							MessageBox msg1 = new MessageBox(new Shell(),SWT.OK);
							msg1.setMessage("Password doesnt match");
							msg1.open();
							txtconfirmpassword.selectAll();
							txtconfirmpassword.setFocus();
							return;
						

						}
				CustomDialog confirm = new CustomDialog(new Shell());
				confirm.SetMessage( "Do you wish to save ?");
				
				int answer = confirm.open();
					 
				if( answer == SWT.YES)
				{
					if(globals.session[7].equals(1))
					{
						if (StartupController.changePasswordOwn(txtusername.getText(), txtoldpassword.getText(), txtnewpassword.getText()))
						{
							MessageBox msg2= new MessageBox(new Shell(),SWT.OK);
							msg2.setMessage("Password Changed successfully");
							msg2.open();
							btnsave.getShell().getDisplay().dispose();
							MainShell ms = new MainShell(display);	
						}
						else
						{
							MessageBox msg = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
							msg.setMessage("Please enter valid Username or Password");
							msg.open();
							txtusername.selectAll();
							txtusername.setFocus();
							
						}
						
					}
					if(btnchangepassword[0].getSelection()==true)
					{
						if (StartupController.changePasswordOwn(txtusername.getText(), txtoldpassword.getText(), txtnewpassword.getText()))
						{
							MessageBox msg2= new MessageBox(new Shell(),SWT.OK);
							msg2.setMessage("Password Changed successfully");
							msg2.open();
							btnsave.getShell().getDisplay().dispose();
							MainShell ms = new MainShell(display);	
						}
						else
						{
							MessageBox msg = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
							msg.setMessage("Please enter valid Username or Password");
							msg.open();
							txtusername.selectAll();
							txtusername.setFocus();
							
						}
					}
					if(btnchangepassword[1].getSelection()==true)
					{
						String cmbusername = cmbusers.getItem(cmbusers.getSelectionIndex());
						int  usernameindex = cmbusername.indexOf("-"); 
						String strusername = cmbusername.substring(0, usernameindex);
						if (StartupController.changePasswordOthers(strusername,txtnewpassword.getText()))
						{
							MessageBox msg2= new MessageBox(new Shell(),SWT.OK);
							msg2.setMessage("Password Changed successfully");
							msg2.open();
							btnsave.getShell().getDisplay().dispose();
							MainShell ms = new MainShell(display);	
						}
						else
						{
							MessageBox msg = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
							msg.setMessage("Please enter valid Username or Password");
							msg.open();
							txtusername.selectAll();
							txtusername.setFocus();
							
						}
					}
				}
				
			}
			
		});
		btncancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				MessageBox msgConfirm = new MessageBox(new Shell(), SWT.YES| SWT.NO| SWT.ICON_QUESTION );
				msgConfirm.setMessage("Are you sure you want to cancel");
				int answer = msgConfirm.open();
				if(answer == SWT.YES)
				{
					btncancel.getShell().getDisplay().dispose();
					MainShell ms = new MainShell(display);
				}
				if(answer== SWT.NO)
				{
					btnsave.setFocus();				
				}
				
			}
			});
		
	}
	public void makeaccessible(Control c)
	{
		/*
		 * getAccessible() method is the method of class Controlwhich is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
		c.getAccessible();
	}


	
	protected void checkSubclass()
	{
		//this is blank method so will disable the check that prevents subclassing of shells.
	}
	

}

