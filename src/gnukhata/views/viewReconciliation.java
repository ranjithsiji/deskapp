package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.StartupController;

import java.awt.Checkbox;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.Text;

public class viewReconciliation extends Composite {
	static Display display;
	String strOrgName;
	String strFromYear;
	String strToYear;
	String strype;
	String accountName;
	String fromDate;
	String toDate;
	String searchText = "";
	long searchTexttimeout = 0;
	String bankacc;
	String[] financialYear;
	
	
	
	
	Label lblorgName;
	Label selectProj;
	Combo dropbankname;
	Combo dropProjName;
	boolean narration;
	
	Label lblPeriod;
	Label lblFromDt;
	Label lblToDt;
	Label lblFinancialYear;
	
	Text txtFromDtDay;
	Text txtToDtDay;
	Text txtFromDtMonth;
	Text txtToDtMonth;
	Text txtFromDtYear;
	Text txtToDtYear;
	
	Label lblFromDash1;
	Label lblFromDash2;
	Label lblToDash1;
	Label lblToDash2;
	Label lblFeatures;
	Label lblLogo;
	Label lblLink ;
	Label lblLine;
	
	Button btnCheck;
	//Combo dropdownProjectName;
	Button btnView;
	Button btnBack;
	long wait=0;
		
	
	public viewReconciliation(Composite parent,int style) 
	{
		super(parent,style);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		FormData layout = new FormData();
		MainShell.lblLogo.setVisible(false);
		 MainShell.lblLine.setVisible(false);
		 MainShell.lblOrgDetails.setVisible(false);
		 strFromYear =  globals.session[2].toString();
		// narration=narrationflag1;
		strToYear =  globals.session[3].toString();
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(9);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		/*lblLogo.setLocation(getClientArea().width, getClientArea().height);*/
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 10, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
		
		Label lblviewreco = new Label(this, SWT.NONE);
		lblviewreco.setText("Reconciliation:");
		
		lblviewreco.setFont(new Font(display, "Times New Roman", 13, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,28);
		layout.left = new FormAttachment(40);
		//layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(36);
		lblviewreco.setLayoutData(layout);
		
		Label lblbankname= new Label(this,SWT.NONE);
		lblbankname.setText("Bank Name:");
		lblbankname.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
		layout=new FormData();
		layout.top=new FormAttachment(lblviewreco,20);
		layout.left=new FormAttachment(32);
		lblbankname.setLayoutData(layout);
		
		
		dropbankname = new Combo(this, SWT.DROP_DOWN | SWT.READ_ONLY);
		dropbankname.setFont(new Font(display, "Times New Roman", 9, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblviewreco,20);
		layout.left = new FormAttachment(lblbankname,20);
		layout.right = new FormAttachment(60);
		layout.bottom = new FormAttachment(15);
		dropbankname.setLayoutData(layout);
		//dropbankname.setFocus();
		
		
		Label lblPeriod=new Label(this, SWT.NONE);
		lblPeriod.setText("Period:");
		lblPeriod.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		layout=new FormData();
		layout.top=new FormAttachment(lblbankname,35);
		layout.left=new FormAttachment(32);
		lblPeriod.setLayoutData(layout);
	
		Label lblFromDt=new Label(this, SWT.NONE);
		lblFromDt.setText("&From :");
		lblFromDt.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		layout=new FormData();
		layout.top =new FormAttachment(lblbankname,35);
		layout.left=new FormAttachment(lblPeriod,11);		
		lblFromDt.setLayoutData(layout);
	    
		txtFromDtDay=new Text(this, SWT.BORDER);
		txtFromDtDay.setMessage("dd");
		txtFromDtDay.setText(strFromYear.substring(0,2));
		txtFromDtDay.setTextLimit(2);
		txtFromDtDay.setFont(new Font(display, "Times New Roman", 9, SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(lblbankname,35);
		layout.left = new FormAttachment(lblFromDt, 10);
		txtFromDtDay.setLayoutData(layout);
		txtFromDtDay.setVisible(true);
				
		lblFromDash1 = new Label(this, SWT.NONE);
		lblFromDash1.setText("-");
		lblFromDash1.setFont(new Font(display, "Time New Roman", 12, SWT.BOLD));
		layout = new FormData();
		layout.left = new FormAttachment(txtFromDtDay,2);
		layout.top = new FormAttachment(lblbankname,35);
		lblFromDash1.setLayoutData(layout);
		lblFromDash1.setVisible(true);
		
		txtFromDtMonth=new Text(this, SWT.BORDER);
		txtFromDtMonth.setMessage("mm");
		txtFromDtMonth.setText(strFromYear.substring(3,5));
		txtFromDtMonth.setTextLimit(2);
		txtFromDtMonth.setFont(new Font(display, "Times New Roman", 9, SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(lblbankname,35);
		layout.left = new FormAttachment(lblFromDash1, 3);
		txtFromDtDay.setTabs(1);
		txtFromDtMonth.setLayoutData(layout);
		txtFromDtMonth.setVisible(true);
		
		
		lblFromDash2 = new Label(this, SWT.NONE);
		lblFromDash2.setText("-");
		lblFromDash2.setFont(new Font(display, "Time New Roman", 12, SWT.BOLD));
		layout = new FormData();
		layout.left = new FormAttachment(txtFromDtMonth,3);
		layout.top = new FormAttachment(lblbankname,35);
		lblFromDash2.setLayoutData(layout);
		lblFromDash2.setVisible(true);
		
		
		txtFromDtYear=new Text(this, SWT.BORDER);
		txtFromDtYear.setMessage("yyyy");
		txtFromDtYear.setTextLimit(4);
		//txtFromDtYear.setTextLimit(4);
		txtFromDtYear.setText(strFromYear.substring(6));
		txtFromDtYear.setFont(new Font(display, "Times New Roman", 9, SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(lblbankname,35);
		layout.left = new FormAttachment(lblFromDash2, 2);
		txtFromDtYear.setLayoutData(layout);
		
		Label lblToDt=new Label(this, SWT.NONE);
		lblToDt.setText("T&o :");
		lblToDt.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		layout=new FormData();
		layout.top =new FormAttachment(lblFromDt, 14);
		layout.left=new FormAttachment(lblPeriod,11);
		lblToDt.setLayoutData(layout);
		
		txtToDtDay=new Text(this, SWT.BORDER);
		txtToDtDay.setMessage("dd");
		txtToDtDay.setText(strToYear.substring(0,2));
		txtToDtDay.setTextLimit(2);
		txtToDtDay.setFont(new Font(display, "Times New Roman", 9, SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(lblFromDt, 14);
		layout.left = new FormAttachment(lblFromDt, 10);
		txtToDtDay.setLayoutData(layout);
		txtToDtDay.setVisible(true);
		
		Label lblToDash1 = new Label(this, SWT.NONE);
		lblToDash1.setText("-");
		lblToDash1.setFont(new Font(display, "Time New Roman", 12, SWT.BOLD));
		layout = new FormData();
		layout.left = new FormAttachment(txtToDtDay,2);
		layout.top = new FormAttachment(lblFromDt, 14);
		lblToDash1.setLayoutData(layout);
		lblToDash1.setVisible(true);
		
		txtToDtMonth=new Text(this, SWT.BORDER);
		txtToDtMonth.setMessage("mm");
		txtToDtMonth.setText(strToYear.substring(3,5));
		txtToDtMonth.setTextLimit(2);
		txtToDtMonth.setFont(new Font(display, "Times New Roman", 9, SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(lblFromDt, 14);
		layout.left = new FormAttachment(lblFromDash1, 3);
		txtToDtMonth.setLayoutData(layout);
		txtToDtMonth.setVisible(true);
		
		
		Label lblToDash2 = new Label(this, SWT.NONE);
		lblToDash2.setText("-");
		lblToDash2.setFont(new Font(display, "Time New Roman", 12, SWT.BOLD));
		layout = new FormData();
		layout.left = new FormAttachment(txtToDtMonth,3);
		layout.top = new FormAttachment(lblFromDt, 14);
		lblToDash2.setLayoutData(layout);
		lblToDash2.setVisible(true);
		
		
		txtToDtYear=new Text(this, SWT.BORDER);
		txtToDtYear.setMessage("yyyy");
		txtToDtYear.setText(strToYear.substring(6));
		txtToDtYear.setTextLimit(4);
		txtToDtYear.setFont(new Font(display, "Times New Roman", 9, SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(lblFromDt, 14);
		layout.left = new FormAttachment(lblToDash2, 3);
		txtToDtYear.setLayoutData(layout);
		txtToDtYear.setVisible(true);
		
		btnCheck=new Button(this, SWT.CHECK);
		btnCheck.setFont(new Font(display, "Time New Roman", 10, SWT.BOLD));
		btnCheck.setText("Narration");
		layout = new FormData();
		layout.top=new FormAttachment(lblToDt, 20);
		layout.left = new FormAttachment(32);
		btnCheck.setLayoutData(layout);
		btnCheck.setVisible(true);
		
		
		btnView= new Button(this, SWT.PUSH);
		btnView.setText(" &View ");
		btnView.setFont(new Font(display, "Times New Roman", 10,SWT.BOLD));
		//btnView.setEnabled(true);
		layout = new FormData();
		layout.top=new FormAttachment(48);
		layout.left = new FormAttachment(40);
		layout.right = new FormAttachment(48);
		layout.bottom = new FormAttachment(52);
		btnView.setLayoutData(layout);
		
		
		String[] allbank = gnukhata.controllers.reportController.getBankList();
		if(allbank.length==0)
		{
			MessageBox	 msg = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
			msg.setMessage("No Bank Account Found");
			msg.open();
			//btnView.setEnabled(false);
			btnView.getShell().getDisplay().dispose();
			MainShell ms = new MainShell(display);
		}
		if(allbank.length==1)
		{
			for (int i = 0; i < allbank.length; i++ )
			{
				dropbankname.add(allbank[i]);
				dropbankname.setEnabled(false);
				txtFromDtDay.setFocus();
			}
		}
		if(allbank.length > 1)
		{
			dropbankname.add("----------Please select----------");
			for (int i = 0; i < allbank.length; i++ )
			{
				dropbankname.add(allbank[i]);
				dropbankname.setFocus();
				btnView.setEnabled(false);
			}
		}
		
		dropbankname.select(0);	
	
		this.getAccessible();
		this.pack();
		
		this.setEvents();
	
	
}
	private void setEvents()
	{
		//dropbankname.setFocus();
				

		dropbankname.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
				if (dropbankname.getItemCount()==0)
				{
					btnView.setEnabled(true);
					return;
				}
				if(dropbankname.getItem(0).equals("----------Please select----------"))
				{
					btnView.setEnabled(false);
				}
				if(dropbankname.getItemCount() > 0 && dropbankname.getSelectionIndex() > 0)
				{
					btnView.setEnabled(true);
				}
				
				}
				

			
		});
		dropbankname.addKeyListener(new KeyAdapter() {
		
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR || arg0.keyCode == SWT.TAB)
				{
					/*if(!dropbankname.getItem(0).equals("----------Please select----------"))
					{
						txtFromDtDay.setFocus();
						txtFromDtDay.setSelection(0,2);
						btnView.setEnabled(true);
					}*/
					
					if(dropbankname.getItemCount() > 0 && dropbankname.getSelectionIndex()==0)
					{
						MessageBox	 msg = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
						msg.setMessage("Please select the Account name.");
						msg.open();
						dropbankname.setFocus();
					
						btnView.setEnabled(false);
						return;
					}
					if(dropbankname.getItemCount() > 0 && dropbankname.getSelectionIndex() > 0)
					{
						txtFromDtDay.setFocus();
						txtFromDtDay.setSelection(0,2);
						btnView.setEnabled(true);
					}
					
					//btnView.setEnabled(true);
				}
				
				
			}
			});
		
		dropbankname.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				//code here
				if(arg0.keyCode== SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
				{
					dropbankname.notifyListeners(SWT.Selection ,new Event()  );
					//dropdownFinancialYear.setFocus();
					return;
				}
				/*if(!Character.isLetterOrDigit(arg0.character) )
				{
					return;
				}
				
				*/
				long now = System.currentTimeMillis();
				if (now > searchTexttimeout)
				{
			         searchText = "";
			      }
				searchText += Character.toLowerCase(arg0.character);
				searchTexttimeout = now + 500;
				
				for(int i = 0; i < dropbankname.getItemCount(); i++ )
				{
					if(dropbankname.getItem(i).toLowerCase().startsWith(searchText ) ){
						//arg0.doit= false;
						dropbankname.select(i);
						break;
					}
				}
			}
		});
		
		/*dropdownProjectName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				//code here
				if(arg0.keyCode== SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
				{
					dropdownProjectName.notifyListeners(SWT.Selection ,new Event()  );
					//dropdownFinancialYear.setFocus();
					return;
				}
				if(!Character.isLetterOrDigit(arg0.character) )
				{
					return;
				}
				
				
				long now = System.currentTimeMillis();
				if (now > searchTexttimeout)
				{
			         searchText = "";
			      }
				searchText += Character.toLowerCase(arg0.character);
				searchTexttimeout = now + 500;
				
				for(int i = 0; i < dropdownProjectName.getItemCount(); i++ )
				{
					if(dropdownProjectName.getItem(i).toLowerCase().startsWith(searchText ) ){
						//arg0.doit= false;
						dropdownProjectName.select(i);
					}
				}
			}
		});*/
		
		
		/*
		txtFromDtDay.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				
				if(txtFromDtDay.getText().length()==txtFromDtDay.getTextLimit())
				{
					txtFromDtMonth.setFocus();
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					dropbankname.setFocus();
				}
			

			}
		});*/
		
		
		txtFromDtDay.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});
		
		txtFromDtMonth.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});
		
		
		txtFromDtYear.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});

		txtToDtDay.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});	
		
		
		txtToDtMonth.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});
		
		
		txtToDtYear.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});
		
		txtFromDtDay.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
				{
				if(!txtFromDtDay.getText().equals("") && Integer.valueOf ( txtFromDtDay.getText())<10 && txtFromDtDay.getText().length()< txtFromDtDay.getTextLimit())
				{
					txtFromDtDay.setText("0"+ txtFromDtDay.getText());
					//txtFromDtMonth.setFocus();
					txtFromDtDay.setFocus();
					return;	
				}
				txtFromDtMonth.setFocus();
				
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					dropbankname.setFocus();
				}
			

			}
		});
		
		
		/*txtFromDtMonth.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(txtFromDtMonth.getText().length()==txtFromDtMonth.getTextLimit())
				{
					txtFromDtYear.setFocus();
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtFromDtDay.setFocus();
				}
			

			}
		});*/
		
		txtFromDtMonth.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
				{
				if(!txtFromDtMonth.getText().equals("") && Integer.valueOf ( txtFromDtMonth.getText())<10 && txtFromDtMonth.getText().length()< txtFromDtMonth.getTextLimit())
				{
					txtFromDtMonth.setText("0"+ txtFromDtMonth.getText());
					//txtFromDtMonth.setFocus();
					txtFromDtYear.setFocus();
					return;
				}
				txtFromDtYear.setFocus();
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtFromDtDay.setFocus();
				}
				
			

			}
		});
		
		
	txtFromDtYear.addKeyListener(new KeyAdapter() {
		/*@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(txtFromDtYear.getText().length()==txtFromDtYear.getTextLimit())
			{
				txtToDtDay.setFocus();
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtFromDtMonth.setFocus();
			}
		

		}*/
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.CR ||arg0.keyCode == SWT.KEYPAD_CR)
			{
				txtToDtDay.setFocus();
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtFromDtMonth.setFocus();
			}
		}
	});
		
	txtFromDtYear.addFocusListener(new FocusAdapter() {
		
		public void focusLost(FocusEvent args0) {
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				Date voucherDate = sdf.parse(txtFromDtYear.getText() + "-" + txtFromDtMonth.getText() + "-" + txtFromDtDay.getText());
				Date fromDate = sdf.parse(globals.session[2].toString().substring(6)+ "-" + globals.session[2].toString().substring(3,5) + "-"+ globals.session[2].toString().substring(0,2));
				Date toDate = sdf.parse(globals.session[3].toString().substring(6)+ "-" + globals.session[3].toString().substring(3,5) + "-"+ globals.session[3].toString().substring(0,2));
				
				if(voucherDate.compareTo(fromDate)< 0 || voucherDate.compareTo(toDate) > 0 )
				{
					MessageBox errMsg = new MessageBox(new Shell(),SWT.ERROR |SWT.OK );
					errMsg.setMessage("Please enter the date within the financial year");
					errMsg.open();
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
						txtFromDtDay.setFocus();
						txtFromDtDay.selectAll();
						
						}
					});
					
					return;
				}
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.getMessage();
			}
		}
	});
	
txtToDtYear.addFocusListener(new FocusAdapter() {
		
		public void focusLost(FocusEvent args0) {
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				Date voucherDate = sdf.parse(txtToDtYear.getText() + "-" + txtToDtMonth.getText() + "-" + txtToDtDay.getText());
				Date fromDate = sdf.parse(globals.session[2].toString().substring(6)+ "-" + globals.session[2].toString().substring(3,5) + "-"+ globals.session[2].toString().substring(0,2));
				Date toDate = sdf.parse(globals.session[3].toString().substring(6)+ "-" + globals.session[3].toString().substring(3,5) + "-"+ globals.session[3].toString().substring(0,2));
				
				if(voucherDate.compareTo(fromDate)< 0 || voucherDate.compareTo(toDate) > 0 )
				{
					MessageBox errMsg = new MessageBox(new Shell(),SWT.ERROR |SWT.OK );
					errMsg.setMessage("Please enter the date within the financial year");
					errMsg.open();
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
						txtToDtDay.setFocus();
						txtToDtDay.selectAll();
						
						}
					});
					
					return;
				}
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.getMessage();
			}
		}
	});
	
	/*txtToDtDay.addKeyListener(new KeyAdapter() {
		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(txtToDtDay.getText().length()==txtToDtDay.getTextLimit())
			{
				txtToDtMonth.setFocus();
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtFromDtYear.setFocus();
			}
		

		}
	});*/
	

	txtToDtDay.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
			{
			if(!	txtToDtDay.getText().equals("") && Integer.valueOf ( txtToDtDay.getText())<10 && 	txtToDtDay.getText().length()< 	txtToDtDay.getTextLimit())
			{
				txtToDtDay.setText("0"+ txtToDtDay.getText());
				//txtFromDtMonth.setFocus();
				txtToDtMonth.setFocus();
				return;
			}
			txtToDtMonth.setFocus();
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtFromDtYear.setFocus();
			}

		}
	});

	btnView.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				btnCheck.setFocus();
			}
		}
	});

	
		/*txtToDtMonth.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(txtToDtMonth.getText().length()==txtToDtMonth.getTextLimit())
				{
					txtToDtYear.setFocus();
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtToDtDay.setFocus();
				}
			

			}
		});*/
		

		txtToDtMonth.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
				{
				if(!txtToDtMonth.getText().equals("") && Integer.valueOf ( txtToDtMonth.getText())<10 && txtToDtMonth.getText().length()< 	txtToDtMonth.getTextLimit())
				{
					txtToDtMonth.setText("0"+ txtToDtMonth.getText());
					//txtFromDtMonth.setFocus();
					txtToDtYear.setFocus();
					return;
				}
				txtToDtYear.setFocus();
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtToDtDay.setFocus();
				}

			}
		});

		txtToDtYear.addKeyListener(new KeyAdapter() {
			/*@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(txtToDtYear.getText().length()==txtToDtYear.getTextLimit())
				{
					btnCheck.setFocus();
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtToDtMonth.selectAll();
					txtToDtMonth.setFocus();
				}
			

			}*/
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
				{
					btnCheck.setFocus();
				}
				
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtToDtMonth.selectAll();
					txtToDtMonth.setFocus();
				}
			}
		});
		
		
		txtFromDtDay.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(
					KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
						arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		
		txtToDtDay.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
						arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
				{
					arg0.doit = true;
					
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		
		txtToDtMonth.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
						arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		
		
		txtToDtYear.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
						arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		
		txtFromDtMonth.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
						arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		
		txtFromDtYear.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
						arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		
		txtFromDtDay.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				if(!txtFromDtDay.getText().equals("") && (Integer.valueOf(txtFromDtDay.getText())> 31 || Integer.valueOf(txtFromDtDay.getText()) <= 0) )
				{
					MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
					msgdateErr.setMessage("You have entered an invalid date");
					msgdateErr.open();
					
					txtFromDtDay.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtFromDtDay.setFocus();
							
						}
					});
					return;
				}
				if(!txtFromDtDay.getText().equals("") && Integer.valueOf ( txtFromDtDay.getText())<10 && txtFromDtDay.getText().length()< txtFromDtDay.getTextLimit())
				{
					txtFromDtDay.setText("0"+ txtFromDtDay.getText());
					//txtFromDtMonth.setFocus();
					return;
					
					
					
				}
				if(txtFromDtDay.getText().equals(""))
				{
					txtFromDtDay.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtFromDtDay.setFocus();
							
						}
					});
					return;
			
				}
				/*if(txtFromDtDay.getText().length()==2)
				   {
					   txtFromDtMonth.setFocus();
				   }*/
			}
		});
		
		txtFromDtMonth.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				if(!txtFromDtMonth.getText().equals("") && (Integer.valueOf(txtFromDtMonth.getText())> 12 || Integer.valueOf(txtFromDtMonth.getText()) <= 0))
				{
					MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
					msgdateErr.setMessage("You have entered an invalid month");
					msgdateErr.open();
				
					
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtFromDtMonth.setText("");
							txtFromDtMonth.setFocus();
							
						}
					});
					return;
					
				}
				if(! txtFromDtMonth.getText().equals("") && Integer.valueOf ( txtFromDtMonth.getText())<10 && txtFromDtMonth.getText().length()< txtFromDtMonth.getTextLimit())
				{
					txtFromDtMonth.setText("0"+ txtFromDtMonth.getText());
					return;
				}
				if(txtFromDtMonth.getText().equals(""))
				{
					
					//txtFromDtMonth.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtFromDtMonth.setFocus();
							
						}
					});
					return;
			
				}
				
			}
		});
		
		
		
			txtToDtDay.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				if(!txtToDtDay.getText().equals("") && Integer.valueOf ( txtToDtDay.getText())<10 && txtToDtDay.getText().length()< txtToDtDay.getTextLimit())
				{
					txtToDtDay.setText("0"+ txtToDtDay.getText());
					return;
					
				}
				if(!txtToDtDay.getText().equals("") && (Integer.valueOf(txtToDtDay.getText())> 31 || Integer.valueOf(txtToDtDay.getText()) <= 0) )
				{
					MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
					msgdateErr.setMessage("You have entered an invalid date");
					msgdateErr.open();
					
					txtToDtDay.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtToDtDay.setFocus();
							
						}
					});
					return;
				}
				if(txtToDtDay.getText().equals(""))
				{
					txtToDtDay.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtToDtDay.setFocus();
							txtToDtDay.selectAll();
						}
					});
					return;
				
				}
	
	}
});


			txtToDtMonth.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					
			// TODO Auto-generated method stub
					//super.focusLost(arg0);
					if(!txtToDtMonth.getText().equals("") && (Integer.valueOf(txtToDtMonth.getText())> 12 || Integer.valueOf(txtToDtMonth.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
						msgdateErr.setMessage("You have entered an invalid month");
						msgdateErr.open();
					
						txtToDtMonth.setFocus();
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtToDtMonth.setText("");
								txtToDtMonth.setFocus();
								
							}
						});
						return;
						
					}
					if(! txtToDtMonth.getText().equals("") && Integer.valueOf ( txtToDtMonth.getText())<10 && txtToDtMonth.getText().length()< txtToDtMonth.getTextLimit())
					{
						txtToDtMonth.setText("0"+ txtToDtMonth.getText());
						return;
					}
					
					if(txtToDtMonth.getText().equals(""))
					{
						//txtToDtDay.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtToDtMonth.setFocus();
								
							}
						});
						return;
					}
					
				}
			});
				
			txtFromDtYear.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					if(!txtFromDtYear.getText().trim().equals("") && Integer.valueOf(txtFromDtYear.getText()) < 0000) 
							{
						MessageBox msgbox = new MessageBox(new Shell(), SWT.OK |SWT.ERROR);
						msgbox.setMessage("You have entered an invalid year");
						msgbox.open();
					
						txtFromDtYear.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtFromDtYear.setFocus();
								txtFromDtYear.selectAll();
								
							}
						});
						return;
					}
					if(txtFromDtYear.getText().equals(""))
					{
						txtFromDtYear.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtFromDtYear.setFocus();
								txtFromDtYear.selectAll();
								
							}
						});
						return;
					}
					
				
					
				}
			});

			txtToDtYear.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					if(!txtToDtYear.getText().trim().equals("") && Integer.valueOf(txtToDtYear.getText()) < 0000) 
							{
						MessageBox msgbox = new MessageBox(new Shell(), SWT.OK |SWT.ERROR);
						msgbox.setMessage("You have entered an invalid year");
						msgbox.open();
					
						txtToDtYear.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtToDtYear.setFocus();
								txtToDtYear.selectAll();
								
							}
						});
						return;
					}
					if(txtToDtYear.getText().equals(""))
					{txtToDtYear.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtToDtYear.setFocus();
							txtToDtYear.selectAll();
							
						}
					});
					}
					return;
				}
			});
			
			btnCheck.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
					{	
							btnView.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtToDtYear.setFocus();
						txtToDtYear.selectAll();
					}
				}
			});


		
				
		btnView.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					
					btnCheck.setFocus();
					
				}
			}
		});
		
			btnView.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					Composite grandParent = (Composite) btnView.getParent().getParent();
					String bankname = dropbankname.getItem(dropbankname.getSelectionIndex());
					boolean narrationFlag=btnCheck.getSelection();
					String fromDate= txtFromDtYear.getText() + "-" + txtFromDtMonth.getText() + "-" + txtFromDtDay.getText();
					String toDate = txtToDtYear.getText() + "-" + txtToDtMonth.getText() + "-" + txtToDtDay.getText();

					btnView.getParent().dispose();
					gnukhata.controllers.reportController.showledgerRecon(grandParent, bankname, fromDate, toDate,"No Project",narrationFlag);
					dispose();
					
					
				}
				
			});
		
	
	}
	
	
}