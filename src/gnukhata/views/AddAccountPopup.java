package gnukhata.views;
import java.net.URL;
import java.text.NumberFormat;

import gnukhata.globals;
import gnukhata.controllers.accountController;
import gnukhata.controllers.transactionController;

import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.omg.CORBA.PRIVATE_MEMBER;
 
public class AddAccountPopup extends Dialog {
  String value;
  String suggestedAccountCode = "null";
  Object[] queryParams = new Object[8];
  public static String newAccount;
  public static Boolean cancelflag = false;

  String searchText = "";
  long searchTexttimeout = 0;
	

 
  /**
   * @param parent
   */
  public AddAccountPopup(Shell parent) {
    super(parent);
  }
 
  /**
   * @param parent
   * @param style
   */
  public AddAccountPopup(Shell parent, int style) {
    super(parent, style);
  }
 
  /**
   * Makes the dialog visible.
   *
   * @return
   */
  
  public String open() {
    Shell parent = getParent();
    final Shell shell = new Shell(parent, SWT.TITLE | SWT.BORDER | SWT.APPLICATION_MODAL);
    shell.setText("Add Account");
 
    shell.setLayout(new GridLayout(2, true));
    
 
    Label lblGroupname = new Label(shell, SWT.NULL);
    lblGroupname.setText("&Group Name:");
    
    final Combo dropdownGroupName = new Combo(shell, SWT.DROP_DOWN|SWT.READ_ONLY);
    GridData data = new GridData();
    data.widthHint = 250;
    dropdownGroupName.setLayoutData(data);
    
    
    Label lblSubGroupName = new Label(shell, SWT.NULL);
    lblSubGroupName.setText("S&ub-Group Name:");
    
    
    final Combo dropdownSub_GroupName = new Combo(shell, SWT.VERTICAL| SWT.BORDER  |SWT.READ_ONLY);
    data.widthHint = 250;
    dropdownSub_GroupName.setLayoutData(data);
    
  

   // final Combo dropdownSub_GroupName = new Combo(shell, SWT.READ_ONLY);
    //dropdownSub_GroupName.setSize(34, 10);
    
    final Label lblSubNewGroupName = new Label(shell, SWT.NULL);
    lblSubNewGroupName.setText("N&ew Sub Group");
    lblSubNewGroupName.setVisible(false);
    
    final Text txtSubNewGroupName1 = new Text(shell, SWT.DOUBLE_BUFFERED|SWT.BORDER);
    txtSubNewGroupName1.setVisible(false);
    data.widthHint = 250;
    txtSubNewGroupName1.setLayoutData(data);
   
    Label lblAccname = new Label(shell, SWT.NULL);
    lblAccname.setText("A&ccount Name:");
    
    final Text txtAccountname = new Text(shell, SWT.DOUBLE_BUFFERED| SWT.BORDER);
    data.widthHint = 250;
    txtAccountname.setLayoutData(data);
    
    Label lblAccCode = new Label(shell, SWT.NULL);
    lblAccCode.setText("Account Code:");
    lblAccCode.setVisible(false);
    
    final Text txtAccountCode = new Text(shell, SWT.DOUBLE_BUFFERED| SWT.BORDER);
    data.widthHint = 250;
    txtAccountCode.setLayoutData(data);
    txtAccountCode.setVisible(false);
    
    final Label lblOpeningBalance = new Label(shell, SWT.NULL);
    lblOpeningBalance.setText("&Opening Balance:                  ");
    lblOpeningBalance.setVisible(false);
    
    final Text txtOpeningBalance = new Text(shell, SWT.DOUBLE_BUFFERED|SWT.BORDER);
    data.widthHint = 250;
    txtOpeningBalance.setLayoutData(data);
    txtOpeningBalance.setVisible(false);
    
    final Button btnSave = new Button(shell, SWT.PUSH);
    btnSave.setText("&Save");
    btnSave.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
    
    final Button buttonCancel = new Button(shell, SWT.PUSH);
    buttonCancel.setText("&Cancel");
    /*
    final Combo combo1 = new Combo(shell, SWT.VERTICAL |
    		   SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);
    		  final Combo combo2 = new Combo(shell, SWT.VERTICAL| 
    		   SWT.BORDER  |SWT.READ_ONLY);*/
    
    String[] allgroups = gnukhata.controllers.accountController.getAllGroups();
	dropdownGroupName.add("---Please select---");
	dropdownGroupName.setFocus();
	for (int i = 0; i < allgroups.length; i++ )
	{
		dropdownGroupName.add(allgroups[i]);
		System.out.print("data" + allgroups[i]);
		
	}
	if(globals.session[5].toString().equals("automatic") )
	{
		
		txtAccountCode.setEnabled(false);
	}
dropdownGroupName.select(0);
    /*text.addListener(SWT.Modify, new Listener() {
      public void handleEvent(Event event) {
        try {
          value = new String(text.getText()+ ":7081");

          buttonOK.setEnabled(true);
        } catch (Exception e) {
          buttonOK.setEnabled(true);
        }
      }
    });
    
*/ 
    System.out.println(value); 
    /*btnSave.addListener(SWT.Selection, new Listener() {
      public void handleEvent(Event event) {
			XmlRpcClientConfigImpl	  conf = new XmlRpcClientConfigImpl();
			try {
				conf.setServerURL(new URL(value));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			globals.client.setConfig(conf);

        shell.dispose();
      }
    });
*/ 
      
   
    dropdownGroupName.addSelectionListener(new SelectionAdapter() {
    		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			System.out.print("click event");
			String selectedGroup = dropdownGroupName.getItem(dropdownGroupName.getSelectionIndex());
			String[] subGroups = gnukhata.controllers.accountController.getSubGroups(selectedGroup);
			queryParams[0] = selectedGroup;
			dropdownSub_GroupName.removeAll();
			if(selectedGroup.equals("Capital"))
			{
				suggestedAccountCode = "CP";
			}
			if(selectedGroup.equals("Current Asset"))
			{
				suggestedAccountCode = "CA";
			}
			if(selectedGroup.equals("Fixed Assets"))
			{
				suggestedAccountCode = "FA";
			}
			if(selectedGroup.equals("Direct Income"))
			{
				suggestedAccountCode = "DI";
			}
			if(selectedGroup.equals("Indirect Income"))
			{
				suggestedAccountCode = "II";
			}
			if(selectedGroup.equals("Current Liability"))
			{
				suggestedAccountCode = "CL";
			}
			if(selectedGroup.equals("Loans(Asset)"))
			{
				suggestedAccountCode = "LA";
			}
			if(selectedGroup.equals("Loans(Liability)"))
			{
				suggestedAccountCode = "LL";
			}
			if(selectedGroup.equals("Miscellaneous Expenses(Asset)"))
			{
				suggestedAccountCode = "ME";
			}
			if(selectedGroup.equals("Direct Expense"))
			{
				suggestedAccountCode = "DE";
			}
			if(selectedGroup.equals("Investment"))
			{
				suggestedAccountCode = "IV";
			}
			if(selectedGroup.equals("Reserves"))
			{
				suggestedAccountCode = "RS";
			}
			if(selectedGroup.equals("Indirect Expense"))
			{
				suggestedAccountCode = "IE";
			}
			
			
			dropdownSub_GroupName.add("---Please select---");
			dropdownSub_GroupName.select(0);
			if(dropdownGroupName.getText().trim().equals("Please select"))
			{
				lblOpeningBalance.setText("&Opening Balance :");
			}
			if(selectedGroup.equals("Current Asset") || selectedGroup.equals("Fixed Assets") || selectedGroup.equals("Investment") || selectedGroup.equals("Loans(Asset)"))
			{
				lblOpeningBalance.setText("Debit &Opening Balance ");
			}
			if (selectedGroup.equals("Capital") || selectedGroup.equals("Corpus") || selectedGroup.equals("Current Liability") || selectedGroup.equals("Loans(Liability)") || selectedGroup.equals("Miscellaneous Expenses(Asset)") || selectedGroup.equals("Reserves") )
			{
				lblOpeningBalance.setText("Credit &Opening Balance ");
			}
			if( selectedGroup.equals("Direct Income") || selectedGroup.equals("Indirect Income") || selectedGroup.equals("Direct Expense") || selectedGroup.equals("Indirect Expense") )
			{
				lblOpeningBalance.setEnabled(false);
				
				txtOpeningBalance.setEnabled(false);
				//dropdownSubGroupName.add("No Sub-Group");
			}
			else
			{
				lblOpeningBalance.setEnabled(true);
				txtOpeningBalance.setEnabled(true);
			}

			for (int i = 0; i < subGroups.length; i++ )
				dropdownSub_GroupName.add(subGroups[i]);
			
		}		
	});
    
    dropdownGroupName.addKeyListener(new KeyAdapter() {
		@Override
		public void keyReleased(KeyEvent arg0) {
			//code here
			if(arg0.keyCode== SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
			{if(dropdownGroupName.getSelectionIndex() == 0)
			{
				MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
				msg.setMessage("Please select Group name");
				msg.open();
				dropdownGroupName.setFocus();
			}
			else if(dropdownGroupName.getSelectionIndex() != 0)
			{
			dropdownGroupName.notifyListeners(SWT.Selection, new Event());
			dropdownSub_GroupName.setFocus();
			}
		
		return;
			}
			long now = System.currentTimeMillis();
			if (now > searchTexttimeout){
		         searchText = "";
		      }
			searchText += Character.toLowerCase(arg0.character);
			searchTexttimeout = now + 500;
			
			for(int i = 0; i < dropdownGroupName.getItemCount(); i++ )
			{
				if(dropdownGroupName.getItem(i).toLowerCase().startsWith(searchText ) ){
					dropdownGroupName.select(i);
					break;
				}
			}
			if(arg0.keyCode== SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
			{
				dropdownSub_GroupName.setFocus();
			}
		}
	});

    
	dropdownSub_GroupName.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			//super.widgetSelected(arg0);
			if(dropdownSub_GroupName.getItem(dropdownSub_GroupName.getSelectionIndex()).equals("Create New Sub-Group"))
			{
				lblSubNewGroupName.setVisible(true);
				txtSubNewGroupName1.setVisible(true);
				queryParams[1] = "Create New Sub-Group";
				
			}
			else
			{
				lblSubNewGroupName.setVisible(false);
				txtSubNewGroupName1.setVisible(false);
				queryParams[1] = dropdownSub_GroupName.getItem(dropdownSub_GroupName.getSelectionIndex());
				queryParams[2] = ""; 

			}
		}
	});
	
	dropdownGroupName.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
			//super.keyPressed(e);
			if(e.keyCode== SWT.CR || e.keyCode == SWT.KEYPAD_CR)
			{
				dropdownSub_GroupName.setFocus();
			}
		}
	});
	
	dropdownSub_GroupName.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
			//super.keyPressed(e);
			if(e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR)
			{
				if(dropdownSub_GroupName.getSelectionIndex()< 0)
				{
					MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
					msg.setMessage("Please select Subgroup name");
					msg.open();
					dropdownSub_GroupName.setFocus();
				}
				else if(dropdownSub_GroupName.getSelectionIndex() >= 0)
				{
					if(txtSubNewGroupName1.isVisible())
					{
						txtSubNewGroupName1.setFocus();		
					}
					else if(!txtSubNewGroupName1.isVisible())
					{
						txtAccountname.setFocus();
					}
				}txtAccountname.setFocus();
				
			}
			if(e.keyCode== SWT.ARROW_UP )
			{
				if(dropdownSub_GroupName.getSelectionIndex() == 0)
				{
					dropdownGroupName.setFocus();
				}
			}
			long now = System.currentTimeMillis();
			if (now > searchTexttimeout){
		         searchText = "";
		      }
			searchText += Character.toLowerCase(e.character);
			searchTexttimeout = now + 1000;					
			for(int i = 0; i < dropdownSub_GroupName.getItemCount(); i++ )
			{
				if(dropdownSub_GroupName.getItem(i).toLowerCase().startsWith(searchText ) ){
					//arg0.doit= false;
					dropdownSub_GroupName.select(i);
				}
			}			
			
							
		}
	});
	
	txtSubNewGroupName1.addKeyListener(new KeyAdapter() {
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
			//super.keyPressed(e);
			if(e.keyCode== SWT.CR || e.keyCode == SWT.KEYPAD_CR)
			{
				txtAccountname.setFocus();
			}
			if(e.keyCode== SWT.ARROW_UP)
			{
				dropdownSub_GroupName.setFocus();
			}
		}
		
	});
	
	/*txtAccountCode.addKeyListener(new KeyAdapter() {
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
			//super.keyPressed(e);
			if(e.keyCode== SWT.CR || e.keyCode == SWT.KEYPAD_CR)
			{
				btnSave.setFocus();
				btnSave.notifyListeners(SWT.Selection, new Event());
			}
			if(e.keyCode== SWT.ARROW_UP)
			{
				txtAccountname.setFocus();
			}
		}
		
	});*/
	
	
	txtSubNewGroupName1.addKeyListener(new KeyAdapter() {
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
			//super.keyPressed(e);
			if(e.keyCode== SWT.CR || e.keyCode == SWT.KEYPAD_CR)
			{
				txtAccountname.setFocus();
			}
			if(e.keyCode== SWT.ARROW_UP)
			{
				dropdownSub_GroupName.setFocus();
			}
		}
		
	});
	
	
	txtAccountname.addKeyListener(new KeyAdapter() {
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
			//super.keyPressed(e);
			if(e.keyCode== SWT.CR || e.keyCode == SWT.KEYPAD_CR)
			{
				btnSave.setFocus();
				//btnSave.notifyListeners(SWT.Selection ,new Event() );

				if(txtAccountCode.isVisible())
				{
				txtAccountCode.setFocus();
				}
				else
				{
					btnSave.setFocus();
					//btnSave.notifyListeners(SWT.Selection ,new Event() );
				}
				
			}
		if(e.keyCode== SWT.ARROW_UP)
			{
			   if(dropdownSub_GroupName.getSelectionIndex()==1)
				{
					dropdownSub_GroupName.setFocus();
				}
				
			   if(txtSubNewGroupName1.isVisible())
				{
				   txtSubNewGroupName1.setFocus();
				}
			
				else
				{

					dropdownSub_GroupName.setFocus();
				}
				
			}
		}
		
	});

	btnSave.addKeyListener(new KeyAdapter() {
		public void keyPressed(KeyEvent e){
			
			if(e.keyCode == SWT.ARROW_RIGHT);
			{
				//buttonCancel.setFocus();
				buttonCancel.setFocus();
			}

			if(e.keyCode == SWT.ARROW_UP)
			{
				txtAccountname.setFocus();
			}
		}
		
	});
	

	buttonCancel.addKeyListener(new KeyAdapter() {
		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyReleased(arg0);
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtAccountname.setFocus();
			}			
			else
			{						
				arg0.doit = false;
			}
			if(arg0.keyCode==SWT.ARROW_LEFT)
			{
				btnSave.setFocus();
			}
		}
	});
/*
	buttonCancel.addKeyListener(new KeyAdapter() {
		public void keyPressed(KeyEvent e){
			
			if(e.keyCode == SWT.ARROW_LEFT);
			{
				btnSave.setFocus();
			}
		}
		
	});*/
	
	dropdownGroupName.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0)
		{
			if(dropdownGroupName.getSelectionIndex()==1 || dropdownGroupName.getSelectionIndex()==13 || dropdownGroupName.getSelectionIndex()==12 || dropdownGroupName.getSelectionIndex()==10 || dropdownGroupName.getSelectionIndex()==4 || dropdownGroupName.getSelectionIndex()==5 || dropdownGroupName.getSelectionIndex()==7 || dropdownGroupName.getSelectionIndex()==8)
			{
				try {
					if(dropdownSub_GroupName.getItem(0).contentEquals("---Please select---"))
					{
						dropdownSub_GroupName.remove(0);
						dropdownSub_GroupName.select(0);
						dropdownSub_GroupName.notifyListeners(SWT.Selection,new Event());
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	});
	txtAccountname.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			if(!txtAccountname.getText().trim().equals(""))
			{
				txtAccountname.setText(Character.toUpperCase(txtAccountname.getText().charAt(0)) + txtAccountname.getText().substring(1) );
				String result = accountController.accountExists(txtAccountname.getText());
				if (Integer.valueOf(result) == 1)
				{
					MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
					msg.setMessage("The account name you entered already exists, please choose another name.");
					msg.open();
					txtAccountname.setText("");
					txtAccountname.setFocus();
					return;
				}
				else
				{
					queryParams[3] = txtAccountname.getText();
				}
				if(globals.session[5].toString().equals("manually") )
				{
					queryParams[4] = "manually";
					suggestedAccountCode = suggestedAccountCode + txtAccountname.getText().substring(0,1);
					suggestedAccountCode = accountController.getSuggestedAccountCode(suggestedAccountCode);
					txtAccountCode.setText(suggestedAccountCode);
					queryParams[7] = suggestedAccountCode;
				}
				else
				{
					queryParams[7] = "";
				}
			}

		}
	});

/*	txtAccountname.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
			//super.keyPressed(e);
			if(e.keyCode== SWT.CR || e.keyCode == SWT.KEYPAD_CR)
			{
				if(txtOpeningBalance.isEnabled())
				{
					txtOpeningBalance.setFocus();
				}
				else
				{
					btnSave.setFocus();
					btnSave.notifyListeners(SWT.Selection, new Event());
					
				
				}



			}
			if(e.keyCode== SWT.ARROW_UP)
			{
				dropdownSub_GroupName.setFocus();
			}
		}
	});*/
	
	    
		btnSave.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			queryParams[4] = globals.session[5];
			//queryParams[7] = suggestedAccountCode;
			//super.widgetSelected(arg0);
			if(dropdownGroupName.getSelectionIndex()== 0 )
			{
				MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
				errMessage.setText("Error!");
				errMessage.setMessage("Please select a group for this account");
				errMessage.open();
				dropdownGroupName.setFocus();
				return;
			}
			if(dropdownSub_GroupName.getText().trim().equals("Please select") )
			{
				MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
				errMessage.setText("Error!");
				errMessage.setMessage("Please select a Sub-group or NO Sub-Group for this account");
				errMessage.open();
				dropdownSub_GroupName.setFocus();
				return;
			}
			if(txtAccountname.getText().trim().equals("") )
			{
				MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
				errMessage.setText("Error!");
				errMessage.setMessage("Please enter an Account Name");
				errMessage.open();
				txtAccountname.setFocus();

				return;
			}
			
			if ( dropdownSub_GroupName.getText().equals("Create New Sub-Group") && txtSubNewGroupName1.getText().trim().equals(""))
			{
				MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
				msg.setText("Error");
				msg.setMessage("Please enter an New Sub-Group Name");
				msg.open();
				txtSubNewGroupName1.setFocus();
				return;
			}
			if(txtSubNewGroupName1.getVisible())
			{
				queryParams[2] = txtSubNewGroupName1.getText();
				
			}
			if(txtOpeningBalance.getText().trim().equals("")|| txtOpeningBalance.getEnabled()== false )
			{
				queryParams[5] = "0.00";
				queryParams[6] = "0.00";
				
			}
			else
			{
				NumberFormat nf = NumberFormat.getInstance();
				nf.setMaximumFractionDigits(2);
				nf.setMinimumFractionDigits(2);
				nf.setGroupingUsed(false);

				queryParams[5] = nf.format(Double.valueOf(txtOpeningBalance.getText()));
				queryParams[6] = nf.format(Double.valueOf(txtOpeningBalance.getText()));
			}
			/*for (int i=0; i < queryParams.length; i++ )
			{
				MessageBox msg = new MessageBox(new Shell(), SWT.OK );
				msg.setMessage(queryParams[i].toString());
				msg.open();
			}*/
			if(accountController.setAccount(queryParams))
			{
				/*MessageBox successMsg = new MessageBox(new Shell(),SWT.OK| SWT.ICON_INFORMATION);
				successMsg.setText("success");
				successMsg.setMessage("Account "+ txtAccountname.getText() + " added successfully" );
				successMsg.open();*/
				
				newAccount = txtAccountname.getText();
				dropdownGroupName.select(0);
				dropdownSub_GroupName.select(0);
				txtAccountname.setText("");
				txtAccountCode.setText("");
				txtOpeningBalance.setText("0.00");
				//txtTotalDrOpeningBalance.setText("0.00");
				//txtTotalCrOpeningBalance.setText("0.00");
				txtSubNewGroupName1.setText("");
				txtSubNewGroupName1.setVisible(false);
				lblSubNewGroupName.setVisible(false);
				dropdownGroupName.setFocus();
				
				
				
				
			}
			else
			{
				MessageBox msg = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
				msg.setMessage("The account could not be saved, plese try again.");
				msg.open();
			}
			
		
/*			
			if (typeFlag.equals("Contra")) {
				comboDr_CrAccName1.setItems(transactionController.getContra());
				comboDr_CrAccName2.setItems(transactionController.getContra());

			}*/

			/*Double totalDrOpeningBalance = 0.00;
			Double totalCrOpeningBalance = 0.00;
			Double diffBalance = 0.00;
			
			totalCrOpeningBalance= accountController.getTotalCr();
			totalDrOpeningBalance = accountController.getTotalDr();
			*/
			//txtTotalCrOpeningBalance.setText( totalCrOpeningBalance.toString());
			//txtTotalDrOpeningBalance.setText( totalDrOpeningBalance.toString());
			//diffBalance = totalCrOpeningBalance - totalDrOpeningBalance;
			//txtDiffInOpeningBalance.setText(diffBalance.toString());
			shell.dispose();
					
			
			
		}
	});
    
    txtSubNewGroupName1.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
		// TODO Auto-generated method stub
			//super.focusLost(arg0);
			String result = accountController.subgroupExists(txtSubNewGroupName1.getText());
			MessageBox msg = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
			if( Integer.valueOf(result) == 1)
			{
				msg.setMessage("The subgroup name you entered already exists");
				msg.open();
				txtSubNewGroupName1.setText("");
				txtSubNewGroupName1.setFocus();
			}
			}
	});

    buttonCancel.addListener(SWT.Selection, new Listener() {
      public void handleEvent(Event event) {
        value = null;
        cancelflag = true;
        shell.dispose();
      }
    });
   // text.setText("");
    shell.pack();
    shell.open();
    
 
    Display display = parent.getDisplay();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch())
        display.sleep();
    }
 
    return value;
  }
 
  public static void main(String[] args) {
  
  }
}